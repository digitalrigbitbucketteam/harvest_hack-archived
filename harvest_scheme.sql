/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

-- Dumping database structure for Harvest
CREATE DATABASE IF NOT EXISTS `Harvest` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `Harvest`;

-- Dumping structure for table Harvest.project
CREATE TABLE IF NOT EXISTS `project` (
   `id` bigint(20) NOT NULL,
   `client_id` bigint(20) NOT NULL,
   `name` varchar(255),
   `code` varchar(255) DEFAULT NULL,
   `active` tinyint(1) DEFAULT NULL,
   `billable` tinyint(1) DEFAULT NULL,
   `bill_by` varchar(255),
   `hourly_rate` int(11) DEFAULT NULL,
   `budget` int(11) DEFAULT NULL,
   `budget_by` varchar(255),
   `notify_when_over_budget` tinyint(1) DEFAULT NULL,
   `over_budget_notification_percentage` int(11) DEFAULT NULL,
   `over_budget_notified_at` int(11) DEFAULT NULL,
   `show_budget_to_all` tinyint(1) DEFAULT NULL,
   `starts_on` date DEFAULT NULL,
   `ends_on` date DEFAULT NULL,
   `estimate` int(11) DEFAULT NULL,
   `estimate_by` varchar(255) DEFAULT NULL,
   `hint_earliest_record_at` date DEFAULT NULL,
   `hint_latest_record_at` date DEFAULT NULL,
   `notes` varchar(4096) DEFAULT NULL,
   `cost_budget` int(11) DEFAULT NULL,
   `cost_budget_include_expenses` tinyint(1) DEFAULT NULL,
   `created_at` timestamp NOT NULL,
   `updated_at` timestamp NOT NULL,
  PRIMARY KEY (`id`),
  KEY `client_id` (`client_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


-- Dumping structure for table Harvest.people
CREATE TABLE IF NOT EXISTS `people` (
   `id` bigint(20) NOT NULL,
   `email` varchar(255),
   `is_admin` tinyint(1) DEFAULT NULL,
   `first_name` varchar(255) DEFAULT NULL,
   `last_name` varchar(255) DEFAULT NULL,
   `timezone` varchar(255) DEFAULT NULL,
   `is_contractor` tinyint(1) DEFAULT NULL,
   `telephone` varchar(255) DEFAULT NULL,
   `is_active` tinyint(1) DEFAULT NULL,
   `has_access_to_all_future_projects` tinyint(1) DEFAULT NULL,
   `default_hourly_rate` int(11) DEFAULT NULL,
   `department` varchar(255) DEFAULT NULL,
   `wants_newsletter` tinyint(1) DEFAULT NULL,
   `created_at` timestamp NOT NULL,
   `updated_at` timestamp NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


-- Dumping structure for table Harvest.client
CREATE TABLE IF NOT EXISTS `client` (
   `id` bigint(20) NOT NULL,
   `name` varchar(255),
   `active` tinyint(1) DEFAULT NULL,
   `currency` varchar(255) DEFAULT NULL,
   `highrise_id` varchar(255) DEFAULT NULL,
   `cache_version` bigint(20) NOT NULL,
   `statement_key` varchar(255) DEFAULT NULL,
   `default_invoice_kind` varchar(255) DEFAULT NULL,
   `default_invoice_timeframe` varchar(255) DEFAULT NULL,
   `address` varchar(255) DEFAULT NULL,
   `currency_symbol` varchar(255) DEFAULT NULL,
   `details` varchar(255) DEFAULT NULL,
   `last_invoice_kind` varchar(255) DEFAULT NULL,
   `created_at` timestamp NOT NULL,
   `updated_at` timestamp NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


-- Dumping structure for table Harvest.invoice
CREATE TABLE IF NOT EXISTS `invoice` (
   `id` bigint(20) NOT NULL,
   `client_id` bigint(20) NOT NULL,
   `period_start` date DEFAULT NULL,
   `period_end` date DEFAULT NULL,
   `number` varchar(255),
   `issued_at` date DEFAULT NULL,
   `due_at` date DEFAULT NULL,
   `amount` float DEFAULT NULL,
   `currency` varchar(255) DEFAULT NULL,
   `state` varchar(255) DEFAULT NULL,
   `notes` varchar(4096) DEFAULT NULL,
   `purchase_order` varchar(255) DEFAULT NULL,
   `due_amount` float DEFAULT NULL,
   `due_at_human_format` varchar(255) DEFAULT NULL,
   `subject` varchar(255) DEFAULT NULL,
   `recurring_invoice_id` varchar(255) DEFAULT NULL,
   `tax` int(11) DEFAULT NULL,
   `tax_amount` float DEFAULT NULL,
   `tax2` int(11) DEFAULT NULL,
   `tax2_amount` float DEFAULT NULL,
   `client_key` varchar(255) DEFAULT NULL,
   `estimate_id` varchar(255) DEFAULT NULL,
   `discount` int(11) DEFAULT NULL,
   `discount_amount` float DEFAULT NULL,
   `retainer_id` int(11) DEFAULT NULL,
   `created_by_id` int(11) DEFAULT NULL,
   `project_id` int(11) DEFAULT NULL,
   `client_name` varchar(255) DEFAULT NULL,
   `created_at` timestamp NOT NULL,
   `updated_at` timestamp NOT NULL,
  PRIMARY KEY (`id`),
  KEY `client_id` (`client_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


-- Dumping structure for table Harvest.task
CREATE TABLE IF NOT EXISTS `task` (
   `id` bigint(20) NOT NULL,
   `name` varchar(255),
   `billable_by_default` tinyint(1) DEFAULT NULL,
   `is_default` tinyint(1) DEFAULT NULL,
   `default_hourly_rate` int(11) DEFAULT NULL,
   `deactivated` tinyint(1) DEFAULT NULL,
   `created_at` timestamp NOT NULL,
   `updated_at` timestamp NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping structure for table Harvest.assignment
CREATE TABLE IF NOT EXISTS `assignment` (
   `id` bigint(20) NOT NULL,
   `user_id` bigint(20) NOT NULL,
   `project_id` bigint(20) NOT NULL,
   `is_project_manager` tinyint(1) DEFAULT NULL,
   `deactivated` tinyint(1) DEFAULT NULL,
   `hourly_rate` int(11) DEFAULT NULL,
   `budget` varchar(255) DEFAULT NULL,
   `estimate` varchar(255) DEFAULT NULL,
   `created_at` timestamp NOT NULL,
   `updated_at` timestamp NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `project_id` (`project_id`),
  KEY `user_project_id` (`user_id`, `project_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping structure for table Harvest.effort
CREATE TABLE IF NOT EXISTS `effort` (
   `id` bigint(20) NOT NULL AUTO INCREMENT,
   `user_id` bigint(20) NOT NULL,
   `project_id` bigint(20) NOT NULL,
   `task_id` bigint(20) NOT NULL,
   `hours` int(11) DEFAULT NULL,
   `spent_at` date DEFAULT NULL,
   `timer_started_at` varchar(255) DEFAULT NULL,
   `adjustment_record` tinyint(1) DEFAULT NULL,
   `is_closed` tinyint(1) DEFAULT NULL,
   `is_billed` tinyint(1) DEFAULT NULL,
   `is_estimated` tinyint(1) DEFAULT NULL,
   `notes` varchar(4096) DEFAULT NULL,
   `created_at` timestamp NOT NULL,
   `updated_at` timestamp NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `project_id` (`project_id`),
  KEY `task_id` (`task_id`),
  KEY `user_project_id` (`user_id`, `project_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
