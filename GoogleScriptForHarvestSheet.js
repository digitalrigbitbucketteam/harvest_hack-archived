function importHarvestPeopleData() {
  processHarvestResource(PropertiesService.getScriptProperties().getProperty('TALENT_RESOURCE'),
                         PropertiesService.getScriptProperties().getProperty('TALENT_SHEET'),
                         true);
}

function importHarvestClientData() {
  processHarvestResource(PropertiesService.getScriptProperties().getProperty('CLIENT_RESOURCE'),
                         PropertiesService.getScriptProperties().getProperty('CLIENT_SHEET'),
                         true);
}

function importHarvestTaskData() {
  processHarvestResource(PropertiesService.getScriptProperties().getProperty('TASK_RESOURCE'),
                         PropertiesService.getScriptProperties().getProperty('TASK_SHEET'),
                         true);
}

function importHarvestProjectData() {

  var jsonData = processHarvestResource(PropertiesService.getScriptProperties().getProperty('PROJECT_RESOURCE'),
                                        PropertiesService.getScriptProperties().getProperty('PROJECT_SHEET'),
                                        true);

  createOrClearSheet(PropertiesService.getScriptProperties().getProperty('ASSIGNMENT_SHEET'));
  createOrClearSheet(PropertiesService.getScriptProperties().getProperty('EFFORT_SHEET'));
  createOrClearSheet(PropertiesService.getScriptProperties().getProperty('FORECAST_SHEET'));

  jsonData.forEach(function(aProject) {
    // effort
    var resource = PropertiesService.getScriptProperties().getProperty('PROJECT_RESOURCE') +
      '/' + aProject.project.id +
        PropertiesService.getScriptProperties().getProperty('EFFORT_RESOURCE');
    resource =  resource.replace(/&amp;/g, '&');
    var effortData = processHarvestResource(resource,
                                            PropertiesService.getScriptProperties().getProperty('EFFORT_SHEET'),
                                            false);

    // assignments
    resource = PropertiesService.getScriptProperties().getProperty('PROJECT_RESOURCE') +
      '/' + aProject.project.id +
        PropertiesService.getScriptProperties().getProperty('ASSIGNMENT_RESOURCE');
    var assignmentData = processHarvestResource(resource,
                                                PropertiesService.getScriptProperties().getProperty('ASSIGNMENT_SHEET'),
                                                false);
    // estimations
    forecastFutureEffort(aProject, assignmentData, effortData);
  });
}

function updateHarvestPeopleData() {
  updateHarvestResource(PropertiesService.getScriptProperties().getProperty('TALENT_RESOURCE'),
                        PropertiesService.getScriptProperties().getProperty('TALENT_SHEET'),
                        getSheetUpdateTime(PropertiesService.getScriptProperties().getProperty('TALENT_SHEETå')));
}

function updateHarvestClientData() {
  updateHarvestResource(PropertiesService.getScriptProperties().getProperty('CLIENT_RESOURCE'),
                        PropertiesService.getScriptProperties().getProperty('CLIENT_SHEET'),
                        getSheetUpdateTime(PropertiesService.getScriptProperties().getProperty('CLIENT_SHEET')));
}

function updateHarvestTaskData() {
  updateHarvestResource(PropertiesService.getScriptProperties().getProperty('TASK_RESOURCE'),
                        PropertiesService.getScriptProperties().getProperty('TASK_SHEET'),
                        getSheetUpdateTime(PropertiesService.getScriptProperties().getProperty('TASK_SHEET')));
}

function updateHarvestProjectData() {
  createOrClearSheet(PropertiesService.getScriptProperties().getProperty('FORECAST_SHEET'));

  var jsonData = updateHarvestResource(PropertiesService.getScriptProperties().getProperty('PROJECT_RESOURCE'),
                                       PropertiesService.getScriptProperties().getProperty('PROJECT_SHEET'),
                                       getSheetUpdateTime(PropertiesService.getScriptProperties().getProperty('PROJECT_SHEET')));

  jsonData.forEach(function(aProject) {
    // effort
    var resource = PropertiesService.getScriptProperties().getProperty('PROJECT_RESOURCE') +
      '/' + aProject.project.id +
        PropertiesService.getScriptProperties().getProperty('EFFORT_RESOURCE');
    resource =  resource.replace(/&amp;/g, '&');
    var effortData = updateHarvestResource(resource,
                                           PropertiesService.getScriptProperties().getProperty('EFFORT_SHEET'),
                                           getSheetUpdateTime(PropertiesService.getScriptProperties().getProperty('EFFORT_SHEET')));

    // assignments
    resource = PropertiesService.getScriptProperties().getProperty('PROJECT_RESOURCE') +
      '/' + aProject.project.id +
        PropertiesService.getScriptProperties().getProperty('ASSIGNMENT_RESOURCE');
    var assignmentData = updateHarvestResource(resource,
                                               PropertiesService.getScriptProperties().getProperty('ASSIGNMENT_SHEET'),
                                               getSheetUpdateTime(PropertiesService.getScriptProperties().getProperty('ASSIGNMENT_SHEET')));
    // estimations
    forecastFutureEffort(aProject, assignmentData, effortData);
  });
}


function forecastFutureEffort(aProject, assignmentData, effortData) {

  Logger.log('*** FORECAST START ' + aProject.project.id);
  if (!aProject.project.billable) return;
  if (null == aProject.project.ends_on) return;

  var today = dateFormatIWant((new Date()).toString());
  var dateArray = createDayArray(today, aProject.project.ends_on);

  Logger.log('*** FORECAST Today ' + today + ' till ' + aProject.project.ends_on + ' for ' + dateArray.length + ' DAYS');

  if (dateArray.length < 1) return;

  // sum time spent by user id
  var timeSpent = [];
  effortData.forEach(function(timeEntry) {
    if (!(timeEntry.day_entry.user_id in timeSpent)) {
      timeSpent[timeEntry.day_entry.user_id] = 0;
    }
    timeSpent[timeEntry.day_entry.user_id] += timeEntry.day_entry.hours;
  });

  // determine time left for each user
  // determine time per day for each user
  var timeLeftPerDay = [];
  var lessThanAnHourADay = [];
  assignmentData.forEach(function(anAssignment) {
    if (!(anAssignment.user_assignment.user_id in timeSpent)) {
      timeSpent[anAssignment.user_assignment.user_id] = 0;
    }
    var timeLeft = anAssignment.user_assignment.budget - timeSpent[anAssignment.user_assignment.user_id];
    if (timeLeft > dateArray.length) {
      timeLeftPerDay[anAssignment.user_assignment.user_id] = Math.round(timeLeft / dateArray.length);
    } else if (timeLeft > 0) {
      timeLeftPerDay[anAssignment.user_assignment.user_id] = 0;
      lessThanAnHourADay[anAssignment.user_assignment.user_id] = timeLeft;
    }
  });

  // build data
  var forecastData = [];
  var forecastRow = [];
  var resourceType = Object.keys(effortData[0]);
  var resourceKeys = Object.keys(effortData[0][resourceType]);
  var forecastType = 'forecast';

  // for ever person assigned to this billable project, create a forecast of time booking through to the end of the project.
  // If the person does not have a budget in their assignment, there is no forecasting done.
  // if the person has already met or exceeded thier budget, there is no forecasting done.
  // if the remaining hours exceeds the number of days remaining in the contract, spread the time evenly across the remaining days
  // if the remaining hours is less than the number of days remaining in the contract, forecast 8 hours a day until the remaining time is spent.
  assignmentData.forEach(function(anAssignment) {
    if (anAssignment.user_assignment.user_id in timeLeftPerDay) {
      dateArray.forEach(function(aDay) {
        forecastRow = [];
        resourceKeys.forEach(function(aKey) {
          // okay - yes, this is bad.  Ties this code to expecting certain column headers
          // if that changes, this probably breaks.
          switch(aKey) {
            case 'notes':
              forecastRow.push('FORECASTED EFFORT');
              break;
            case 'spent_at':
              forecastRow.push(aDay);
              break;
            case 'hours':
              var hoursToBook = 0;
              if (0 == timeLeftPerDay[anAssignment.user_assignment.user_id]) {
                if (lessThanAnHourADay[anAssignment.user_assignment.user_id] > 8) {
                  hoursToBook = 8;
                  lessThanAnHourADay[anAssignment.user_assignment.user_id] -= 8;
                } else {
                  hoursToBook = lessThanAnHourADay[anAssignment.user_assignment.user_id];
                  lessThanAnHourADay[anAssignment.user_assignment.user_id] = 0;
                }
              } else {
                hoursToBook = timeLeftPerDay[anAssignment.user_assignment.user_id];
              }
              forecastRow.push(hoursToBook);
              break;
            case 'user_id':
              forecastRow.push(anAssignment.user_assignment.user_id);
              break;
            case 'project_id':
              forecastRow.push(anAssignment.user_assignment.project_id);
              break;
            case 'task_id':
              // very dangerous - but I don't want to programatically figure this out right now.
              forecastRow.push(5715688);
              break;
            case 'adjustment_record':
            case 'is_closed':
            case 'is_billed':
              forecastRow.push('FALSE');
              break;
            default:
              forecastRow.push(null);
          }
        });
        forecastData.push(forecastRow);
        return;
      });
    } else {
      // do nothing - either the user doesn't have a budget or they've already exceed time
    }
  });

  if (forecastData.length < 1) return;

  var ss = SpreadsheetApp.getActiveSpreadsheet();
  var sheet = ss.getSheetByName(PropertiesService.getScriptProperties().getProperty('FORECAST_SHEET'));
  var lastRow = sheet.getLastRow();

  // if the sheet is empty, fill in a header row
  if (sheet.getLastRow() <= 1){
    sheet.appendRow(resourceKeys);
    lastRow += 1;
  }

  Logger.log('FORECAST WRITE ' + forecastData.length + " rows added to " + PropertiesService.getScriptProperties().getProperty('FORECAST_SHEET') +  " starting at row " + lastRow);

  var aRange = sheet.getRange(lastRow + 1, 1, forecastData.length, resourceKeys.length);
  aRange.setValues(forecastData);
  logSheetUpdateTime(PropertiesService.getScriptProperties().getProperty('FORECAST_SHEET'));

}

function processHarvestResource(resourceName, targetSheet, clearSheet) {
  var address = 'https://builditglobal.harvestapp.com/';
  var contentType = 'application/json';
  var acceptHeader = 'application/json';
  var authHeader = 'Basic cGF1bC5rYXJzdGVuQHdpcHJvLmNvbTpXaDFwSXRHMDBk'
  var jsonData = '';

  var headers = {
    'Authorization' : authHeader,
    'Accept' : acceptHeader
  }

  var params = {
    'method' : 'get',
    'headers' : headers,
    'contentType' : contentType
  }

  if (clearSheet) {
    createOrClearSheet(targetSheet);
  }

  var url = address + resourceName;
  var response = UrlFetchApp.fetch(url, params);
  jsonData = JSON.parse(response.getContentText());

  Logger.log(jsonData.length + " " + resourceName + " retrieved - HTTP Response Code [" + response.getResponseCode() + "]" );

  fillInSheet(targetSheet, jsonData);
  logSheetUpdateTime(targetSheet);

  return jsonData;
}

// insert the provided data into the named sheet
// this assumes that the data has been JSON.parse'd
// set column headers and then loads each row
// slow, but there doesn't seem to be a bulk mechanism
function fillInSheet(sheetName, data) {

  if (data.length < 1) {
    return;
  }

  var ROW_ID = 'id';
  var CREATED_DATE = 'created_at';
  var UPDATED_DATE = 'updated_at';

  var ss = SpreadsheetApp.getActiveSpreadsheet();
  var sheet = ss.getSheetByName(sheetName);

  var resourceType = Object.keys(data[0]);
  var resourceKeys = Object.keys(data[0][resourceType]);
  var lastRow = sheet.getLastRow();

  // strip out database columns that shouldn't be sent as part of the resource
  var idx = resourceKeys.indexOf(CREATED_DATE);
  resourceKeys.splice(idx, 1);
  idx = resourceKeys.indexOf(UPDATED_DATE);
  resourceKeys.splice(idx, 1);

  // if the sheet is empty, fill in a header row
  if (sheet.getLastRow() <= 1){
    sheet.appendRow(resourceKeys);
    lastRow += 1;
  }

  var rangeContents = [];
  data.forEach(function(aRow) {
    var dataRow = aRow[resourceType];
    var rowContents = [];
    resourceKeys.forEach(function(aKey) {
      rowContents.push(dataRow[aKey]);
    });
    rangeContents.push(rowContents);
  });

  Logger.log(data.length + " rows added to " + sheetName +  " starting at row " + lastRow);

  var aRange = sheet.getRange(lastRow + 1, 1, data.length, resourceKeys.length);
  aRange.setValues(rangeContents);
}

function updateHarvestResource(resourceName, targetSheet, lastRunTime) {
  var address = 'https://builditglobal.harvestapp.com/';
  var contentType = 'application/json';
  var acceptHeader = 'application/json';
  var authHeader = 'Basic cGF1bC5rYXJzdGVuQHdpcHJvLmNvbTpXaDFwSXRHMDBk'
  var jsonData = '';

  var headers = {
    'Authorization' : authHeader,
    'Accept' : acceptHeader
  }

  var params = {
    'method' : 'get',
    'headers' : headers,
    'contentType' : contentType
  }

  var url = address + resourceName;
  var response = UrlFetchApp.fetch(url, params);
  jsonData = JSON.parse(response.getContentText());

  Logger.log(jsonData.length + " " + resourceName + " retrieved - HTTP Response Code [" + response.getResponseCode() + "]" );

  updateSheet(targetSheet, jsonData, lastRunTime);
  logSheetUpdateTime(targetSheet);

  return jsonData;
}

// insert the provided data into the named sheet
// this assumes that the data has been JSON.parse'd
// set column headers and then loads each row
// slow, but there doesn't seem to be a bulk mechanism
function updateSheet(sheetName, data, lastRunTime) {

  if (data.length < 1) {
    return;
  }

  var RESOURCE_ID = 'id';
  var CREATED_DATE = 'created_at';
  var UPDATED_DATE = 'updated_at';
  var createdDate = new Date();
  var updatedDate = new Date();

  var ss = SpreadsheetApp.getActiveSpreadsheet();
  var sheet = ss.getSheetByName(sheetName);

  var resourceType = Object.keys(data[0]);
  var resourceKeys = Object.keys(data[0][resourceType]);
  var lastRow = sheet.getLastRow();
  var resourceIdIdx = resourceKeys.indexOf(RESOURCE_ID)

  // strip out database columns that shouldn't be sent as part of the resource
  var idx = resourceKeys.indexOf(CREATED_DATE);
  resourceKeys.splice(idx, 1);
  idx = resourceKeys.indexOf(UPDATED_DATE);
  resourceKeys.splice(idx, 1);

  var createdContents = [];
  var updatedContents = [];
  data.forEach(function(aRow) {
    var dataRow = aRow[resourceType];
    var rowContents = [];
    resourceKeys.forEach(function(aKey) {
      rowContents.push(dataRow[aKey]);
    });

    createdDate = googleScriptSucks(dataRow[CREATED_DATE]);
    updatedDate = googleScriptSucks(dataRow[UPDATED_DATE]);

    if (createdDate > lastRunTime) {
      createdContents.push(rowContents);
    } else if (updatedDate > lastRunTime) {
      updatedContents.push(rowContents);
    }
  });

  Logger.log(createdContents.length + " rows to add " + sheetName +  " starting at row " + lastRow);
  Logger.log(updatedContents.length + " rows to update " + sheetName);

  if (createdContents.length > 0) {
    var aRange = sheet.getRange(lastRow + 1, 1, createdContents.length, resourceKeys.length);
    aRange.setValues(createdContents);
  }

  // get the id column
  idx = resourceKeys.indexOf(RESOURCE_ID);
  var idRange = sheet.getRange(1, idx + 1, lastRow);
  var idValues = idRange.getValues();

  updatedContents.forEach(function(aRow) {
    Logger.log('UPDATE to [' + aRow[resourceIdIdx] + '] WITH ' + JSON.stringify(aRow));
    for (var rowIdx = 0; rowIdx < idValues.length; rowIdx++) {
      if (idValues[rowIdx][0] == aRow[resourceIdIdx]) {
        Logger.log('Found Row ' + rowIdx + ' to update : values ' + JSON.stringify(aRow));
        var rangeToUpdate = sheet.getRange(rowIdx + 1, 1, 1, resourceKeys.length);
        var tempHack = [aRow];
        rangeToUpdate.setValues(tempHack);
      }
    }
  });
}


function logSheetUpdateTime(sheetName) {
  createSheet('LAST RUN');
  var ss = SpreadsheetApp.getActiveSpreadsheet();
  var sheet = ss.getSheetByName('LAST RUN');
  var lastRow = sheet.getLastRow();

  Logger.log('UPDATE RUN - sheet ' + sheetName + ' LAST ROW' + lastRow);
  if (lastRow < 1) {
    sheet.appendRow([sheetName,(new Date()).toUTCString()]);
    return;
  }

  var sheetRange = sheet.getRange(1, 1, lastRow, 2);
  var sheetValues = sheetRange.getValues();

  Logger.log(' TIME UPDATE - SHEET ' + JSON.stringify(sheetValues));
  var notFound = true;

  for (var rowIdx = 0; rowIdx < sheetValues.length; rowIdx++) {
    if (sheetValues[rowIdx][0] == sheetName) {
      sheetValues[rowIdx][1] = (new Date()).toUTCString();
      sheetRange.setValues(sheetValues);
      notFound = false;
    }
  }
  if (notFound) {
    sheet.appendRow([sheetName,(new Date()).toUTCString()]);
    return;
  }
}

function getSheetUpdateTime(sheetName) {
  var ss = SpreadsheetApp.getActiveSpreadsheet();
  var sheet = ss.getSheetByName('LAST RUN');
  var lastRow = sheet.getLastRow();

  var sheetRange = sheet.getRange(1, 1, lastRow, 2);
  var sheetValues = sheetRange.getValues();

  for (var rowIdx = 0; rowIdx < sheetValues.length; rowIdx++) {
    if (sheetValues[rowIdx][0] == sheetName) {
      return new Date(sheetValues[rowIdx][1]);
    }
  }
  return new Date();

}
// if the sheet named doesn't exist, create it
// if it does, clear it
function createOrClearSheet(sheetName) {
  var ss = SpreadsheetApp.getActiveSpreadsheet();
  var sheet = ss.getSheetByName(sheetName);
  if (sheet != null) {
    sheet.clear();
  } else {
    sheet = ss.insertSheet(sheetName);
  }
}

// if the sheet named doesn't exist, create it
// if it does, clear it
function createSheet(sheetName) {
  var ss = SpreadsheetApp.getActiveSpreadsheet();
  var sheet = ss.getSheetByName(sheetName);
  if (sheet == null) {
    sheet = ss.insertSheet(sheetName);
  }
}

//
//  Which is YYYY-MM-DD
//
function dateFormatIWant(incomming) {
  var tmpDate = new Date(incomming);
  var theDay = tmpDate.getFullYear()+"-"+('0' + (tmpDate.getMonth()+1)).slice(-2)+"-"+('0' + tmpDate.getDate()).slice(-2)
  return theDay;
}

//  it appears that Google Scripts can't deal with ISO dates
//  so create a function to create a date from this example  2016-06-01T14:07:17Z
//  stole this from : https://gist.github.com/dimkalinux/1478408
function googleScriptSucks(isoDateString) {
  // 0 = whole string
  // 1 = year
  // 2 = month
  // 3 = day
  // 4 = whole time part
  // 5 = hour
  // 6 = minute
  // 7 = second
  // 8 = fractional (with dot)
  // 9 = whole timezone (possibly Z)
  // 10 = offset sign (+ or -)
  // 11 = offset hours
  // 12 = offset minutes (with colon)
  // 13 = offset minutes

  var _rxISO = new RegExp(/^(\d{4})-(\d\d)-(\d\d)([T ](\d\d):(\d\d):(\d\d)(\.\d+)?(Z|([+-])(\d\d)(:(\d\d))?)?)?$/);
  var r = _rxISO.exec(isoDateString);

  if (!r)
    return new Date(Date.parse(iso));

  var year = Number(r[1]), month = Number(r[2]) - 1, day = Number(r[3]);
  if (!r[4])
    return new Date(year, month, day);

  var hour = Number(r[5]), minute = Number(r[6]), second = Number(r[7]);
  var ms = r[8]? Number((r[8] + "000").substr(1, 3)): 0;
  if (!r[9])
    return new Date(year, month, day, hour, minute, second, ms);

  var oh = r[11]? Number(r[10]) + Number(r[11]): 0;
  var om = r[13]? Number(r[10]) + Number(r[13]): 0;
  hour -= oh;
  minute -= om;
  return new Date(Date.UTC(year, month, day, hour, minute, second, ms));
}

function createDayArray(start, end) {
  var SATURDAY = 6;
  var SUNDAY = 0;

  var daysArray = [];

  if (null == start || null == end) {
    return daysArray;
  }

  var dateParts = start.split('-');
  var startDate = new Date(dateParts[0], dateParts[1] - 1, dateParts[2]);
  var dateParts = end.split('-');
  var endDate = new Date(dateParts[0], dateParts[1] - 1, dateParts[2]);
  var daysDiff = Math.round((endDate-startDate)/(1000*60*60*24));

  for (var i = 0; i < daysDiff; i++) {
    if (startDate.getDay() != SATURDAY && startDate.getDay() != SUNDAY) {
      daysArray.push(this.dateFormatIWant(startDate));
    }
    startDate.setUTCDate(startDate.getUTCDate() + 1);
  }

  return daysArray;
}


function XperimentprocessEffort() {

  var address = 'https://builditglobal.harvestapp.com/';
  var contentType = 'application/json';
  var acceptHeader = 'application/json';
  var authHeader = 'Basic cGF1bC5rYXJzdGVuQHdpcHJvLmNvbTpXaDFwSXRHMDBk';
  var data = '';

  var headers = {
    'Authorization' : authHeader,
    'Accept' : acceptHeader
  };

  var params = {
    'method' : 'get',
    'headers' : headers,
    'contentType' : contentType
  };

  var resource = PropertiesService.getScriptProperties().getProperty('PROJECT_RESOURCE') +
    '/10281464' +
      PropertiesService.getScriptProperties().getProperty('EFFORT_RESOURCE');
  var resourceName =  resource.replace(/&amp;/g, '&');

  var sheetName = 'BOBSMYUNCLE'
  createOrClearSheet(sheetName);

  var url = address + resourceName;
  var response = UrlFetchApp.fetch(url, params);
  data = JSON.parse(response.getContentText());

  Logger.log(data.length + " " + resourceName + " retrieved - HTTP Response Code [" + response.getResponseCode() + "]" );

  if (data.length < 1) {
    return;
  }

  var formulas = [];
  var julianWeek = ['julian_week', '=year(%s%i)&weeknum(%s%i)'];
  formulas.push(julianWeek);
  var joinKey = ['join_key', '%i%i'];
  formulas.push(joinKey);

  var ROW_ID = 'id';
  var USER_ID = 'user_id';
  var PROJECT_ID = 'project_id';
  var CREATED_DATE = 'created_at';
  var UPDATED_DATE = 'updated_at';
  var DAY_BOOKED = 'spent_at';

  var ss = SpreadsheetApp.getActiveSpreadsheet();
  var sheet = ss.getSheetByName(sheetName);

  var resourceType = Object.keys(data[0]);
  var resourceKeys = Object.keys(data[0][resourceType]);
  var lastRow = sheet.getLastRow();

  // strip out database columns that shouldn't be sent as part of the resource
  var idx = resourceKeys.indexOf(ROW_ID);
  resourceKeys.splice(idx, 1);
  idx = resourceKeys.indexOf(CREATED_DATE);
  resourceKeys.splice(idx, 1);
  idx = resourceKeys.indexOf(UPDATED_DATE);
  resourceKeys.splice(idx, 1);

  // add formulas
  resourceKeys.push(julianWeek[0]);
  resourceKeys.push(joinKey[0]);

  var valueOfA = 'A'.charCodeAt(0);
  var dateCol = String.fromCharCode(valueOfA + resourceKeys.indexOf(DAY_BOOKED));

  // if the sheet is empty, fill in a header row
  if (sheet.getLastRow() <= 1){
    sheet.appendRow(resourceKeys);
    lastRow += 1;
  }

  var currentRow = lastRow + 1;
  var rangeContents = [];
  data.forEach(function(aRow) {
    var dataRow = aRow[resourceType];
    var rowContents = [];
    for (i = 0; i < resourceKeys.length - formulas.length; i++) {
      rowContents.push(dataRow[resourceKeys[i]]);
    };
    rowContents.push(Utilities.formatString(julianWeek[1], dateCol, currentRow, dateCol, currentRow));
    rowContents.push(Utilities.formatString(joinKey[1], dataRow[USER_ID], dataRow[PROJECT_ID]));
    rangeContents.push(rowContents);
    currentRow += 1;
  });

  Logger.log(data.length + " rows added to " + sheetName +  " starting at row " + lastRow);

  var aRange = sheet.getRange(lastRow + 1, 1, data.length, resourceKeys.length);
  aRange.setValues(rangeContents);

}

function XperimentimportMySQLEffort() {
  var address = 'engagementsdbid.c98rnos0n8qv.us-west-1.rds.amazonaws.com:4242'; //End point provided by the RDS Instance
  var user = 'HarvestAdmin'; //Username given while configuring DB instance
  var userPwd = 'H^rv3stAdm!n'; //User password given while configuring DB instance
  var db = 'Harvest'; //Database name to which you want to connect

  var dbUrl  = 'jdbc:mysql://' + address + '/' + db; //Generates the database url to which you can connect to
  var conn = Jdbc.getConnection(dbUrl, user, userPwd);

  var start = new Date();
  var stmt = conn.createStatement();
  var results = stmt.executeQuery('SELECT * FROM effort');
  var metaData = results.getMetaData();
  var numCols = metaData.getColumnCount();

  var ss = SpreadsheetApp.getActiveSpreadsheet();
  var sheet = ss.getSheetByName("effort");
  if (sheet != null) {
    sheet.clear();
  } else {
    sheet = ss.insertSheet("effort");
  }
  ss.setActiveSheet(sheet);

  var headerRow = [];
  for (var column = 0; column < numCols; column++) {
    headerRow.push(metaData.getColumnName(column + 1));
  }
  ss.appendRow(headerRow);

  while (results.next()) {
    var dataRow = [];
    for (var column = 0; column < numCols; column++) {
      dataRow.push(results.getObject(column + 1));
    }
    Logger.log(dataRow);
    ss.appendRow(dataRow);
  }

  results.close();
  stmt.close();

  var end = new Date();
  Logger.log('Time elapsed: %sms', end - start);
}


function onOpen() {
  var ui = SpreadsheetApp.getUi();
  // Or DocumentApp or FormApp.
  ui.createMenu('HarvestData')
  .addSubMenu(ui.createMenu('Update')
              .addItem('Customer', 'updateCustomers')
              .addItem('Task', 'updateTasks')
              .addItem('People', 'updatePeople')
              .addItem('Project', 'updateProjectData')
             )
  .addSeparator()
  .addSubMenu(ui.createMenu('Import')
              .addItem('Customer', 'importCustomers')
              .addItem('Task', 'importTasks')
              .addItem('People', 'importPeople')
              .addItem('Project', 'importProjectData')
             )
  .addToUi();
}

function importPeople() {
  var ui =   SpreadsheetApp.getUi();
  var response = ui.alert('This will clear out the Talent tab. Are you sure?', ui.ButtonSet.YES_NO);

  if (response == ui.Button.YES) {
    try {
      importHarvestPeopleData();
      ui.alert('Import Complete');
    } catch (err) {
      Logger.log('!!! ERROR ' + JSON.stringify(err));
      ui.alert('ERROR !!! ' + JSON.stringify(err));
    }
  }
}

function importCustomers() {
  var ui =   SpreadsheetApp.getUi();
  var response = ui.alert('This will clear out the Clients tab. Are you sure?', ui.ButtonSet.YES_NO);

  if (response == ui.Button.YES) {
    try {
      importHarvestClientData();
      ui.alert('Import Complete');
    } catch (err) {
      Logger.log('!!! ERROR ' + JSON.stringify(err));
      ui.alert('ERROR !!! ' + JSON.stringify(err));
    }
  }
}

function importTasks() {
  var ui =   SpreadsheetApp.getUi();
  var response = ui.alert('This will clear out the Tasks tab. Are you sure?', ui.ButtonSet.YES_NO);

  if (response == ui.Button.YES) {
    try {
      importHarvestTaskData();
      ui.alert('Import Complete');
    } catch (err) {
      Logger.log('!!! ERROR ' + JSON.stringify(err));
      ui.alert('ERROR !!! ' + JSON.stringify(err));
    }
  }
}

function importProjectData() {
  var ui =   SpreadsheetApp.getUi();
  var response = ui.alert('Will completely refresh Projects, Assignments, and Effort.  It will take several minutes. Are you sure?', ui.ButtonSet.YES_NO);

  if (response == ui.Button.YES) {
    try {
      importHarvestProjectData();
      ui.alert('Import Complete');
    } catch (err) {
      Logger.log('!!! ERROR ' + JSON.stringify(err));
      ui.alert('ERROR !!! ' + JSON.stringify(err));
    }
  }
}

function updatePeople() {
  var ui =   SpreadsheetApp.getUi();
  var response = ui.alert('Are you sure?', ui.ButtonSet.YES_NO);

  if (response == ui.Button.YES) {
    try {
      updateHarvestPeopleData();
      ui.alert('Update Complete');
    } catch (err) {
      Logger.log('!!! ERROR ' + JSON.stringify(err));
      ui.alert('ERROR !!! ' + JSON.stringify(err));
    }
  }
}

function updateCustomers() {
  var ui =   SpreadsheetApp.getUi();
  var response = ui.alert('Are you sure?', ui.ButtonSet.YES_NO);

  if (response == ui.Button.YES) {
    try {
      updateHarvestClientData();
      ui.alert('Update Complete');
    } catch (err) {
      Logger.log('!!! ERROR ' + JSON.stringify(err));
      ui.alert('ERROR !!! ' + JSON.stringify(err));
    }
  }
}

function updateTasks() {
  var ui =   SpreadsheetApp.getUi();
  var response = ui.alert('Are you sure?', ui.ButtonSet.YES_NO);

  if (response == ui.Button.YES) {
    try {
      updateHarvestTaskData();
      ui.alert('Update Complete');
    } catch (err) {
      Logger.log('!!! ERROR ' + JSON.stringify(err));
      ui.alert('ERROR !!! ' + JSON.stringify(err));
    }
  }
}

function updateProjectData() {
  var ui =   SpreadsheetApp.getUi();
  var response = ui.alert('This will take several minutes. Are you sure?', ui.ButtonSet.YES_NO);

  if (response == ui.Button.YES) {
    try {
      updateHarvestProjectData();
      ui.alert('Update Complete');
    } catch (err) {
      Logger.log('!!! ERROR ' + JSON.stringify(err));
      ui.alert('ERROR !!! ' + JSON.stringify(err));
    }
  }
}
