'use strict';

var config = require('config');
var log4js = require('log4js');

log4js.configure('config/log4js_config.json', {});
var logger = log4js.getLogger();


exports.createStatusChangeItem = function(statusValue, start, end) {
	var change = { status : statusValue, startDate: start, endDate: end};
	return change;
}

var SATURDAY = 6;
var SUNDAY = 0;

//
//  Which is YYYY-MM-DD because is is the only way I can keep helpful utilities time zone adjusting for me
//
exports.dateFormatIWant = function(incomming) {
	var tmpDate = new Date(incomming);
	var theDay = tmpDate.getFullYear()+"-"+('0' + (tmpDate.getMonth()+1)).slice(-2)+"-"+('0' + tmpDate.getDate()).slice(-2)
	return theDay;
}

exports.createDayArray = function(start, end) {
	var daysArray = [];
	var startDate = new Date(start);
	var endDate = new Date(end);
	var daysDiff = Math.round((endDate-startDate)/(1000*60*60*24));

	logger.debug("createdDayArray of "+daysDiff+ " days from :[" + start + "] to: [" + end + "]");

	for (var i = 0; i < daysDiff; i++) {
		if (startDate.getDay() != SATURDAY && startDate.getDay() != SUNDAY) {
			daysArray.push(this.dateFormatIWant(startDate));
		}
		startDate.setUTCDate(startDate.getUTCDate() + 1);
	}

	return daysArray;
}

exports.createBasicAuthHeader = function(encodedUser) {
  logger.debug("createBasicAuthHeader() User:["+encodedUser+"]");

  var headers = {
  	'Authorization': 'Basic ' +  encodedUser,
  	'Accept':'application/json',
  	'Content-Type':'application/json'};
  return headers;
}
