'use strict';

var config = require('config');
var log4js = require('log4js');
var rest = require('restler');
var hashmap = require('hashmap');

var csvjson = require('csvjson');

log4js.configure('config/log4js_config.json', {});
var logger = log4js.getLogger();

var utils = require("./loader_util");

exports.storeProjects = function(projects) {
	logger.info("storeProjects for " +  projects.length + " projects");

	csvjson.toCSV(JSON.stringify(projects)).save('projects.csv');
}

exports.storeProjectUsers = function(projectUsers) {
	logger.info("storeProjectUserss for " +  projectUsers.length + " project users");

	csvjson.toCSV(JSON.stringify(projectUsers)).save('projectUsers.csv');
}

exports.storeProjectTime = function(projectTime) {
	logger.info("storeProjectTime for " +  projectTime.length + " projectTime time entries");

	csvjson.toCSV(JSON.stringify(projectTime)).save('projectTime.csv');
}

exports.storePeople = function(people) {
	logger.info("storePeople for " +  people.length + " people");

	csvjson.toCSV(JSON.stringify(people)).save('people.csv');
}

exports.storeTasks = function(tasks) {
	logger.info("storeTasks for " +  tasks.length + " tasks");

	csvjson.toCSV(JSON.stringify(tasks)).save('tasks.csv');
}

exports.storeCustomers = function(clients) {
	logger.info("storeCustomers for " +  clients.length + " customers");

	csvjson.toCSV(JSON.stringify(clients)).save('clients.csv');
}

exports.storeInvoices = function(invoices) {
	logger.info("storeInvoices for " +  invoices.length + " invoices");

	csvjson.toCSV(JSON.stringify(invoices)).save('invoices.csv');
}


exports.storeExpenses = function(expenses) {
	logger.info("storeExpenses for " +  expenses.length + " expenses");

	csvjson.toCSV(JSON.stringify(expenses)).save('expenses.csv');
}
