'use strict';

var config = require('config');
var log4js = require('log4js');
var rest = require('restler');

log4js.configure('config/log4js_config.json', {});
var logger = log4js.getLogger();

var utils = require("./loader_util");
//var db = require("./mongo_db");
//var db = require("./csv");
//var db = require("./mysql_db.js");
var db = require("./files.js");

//  Sample Data from Harvest
//
// "project":
// {
//     "id": 10281464,
//     "client_id": 4314423,
//     "name": "CD CI Phase 2",
//     "code": "",
//     "active": true,
//     "billable": true,
//     "bill_by": "People",
//     "hourly_rate": null,
//     "budget": 1528,
//     "budget_by": "project_cost",
//     "notify_when_over_budget": false,
//     "over_budget_notification_percentage": 80,
//     "over_budget_notified_at": null,
//     "show_budget_to_all": true,
//     "created_at": "2016-03-15T13:23:04Z",
//     "updated_at": "2016-04-16T16:36:16Z",
//     "starts_on": "2016-02-17",
//     "ends_on": "2016-05-18",
//     "estimate": 1528,
//     "estimate_by": "project_cost",
//     "hint_earliest_record_at": "2016-02-15",
//     "hint_latest_record_at": "2016-05-13",
//     "notes": "TRACE : OPP000138239  CRM: 50111504",
//     "cost_budget": 191815,
//     "cost_budget_include_expenses": false
// }
//
// 	"user":
// 	{
//     "id": 1244975,
//     "email": "A0052553@wipro.com",
//     "created_at": "2016-03-22T12:41:54Z",
//     "is_admin": true,
//     "first_name": "Alister",
//     "last_name": "Hodge",
//     "timezone": "London",
//     "is_contractor": true,
//     "telephone": "",
//     "is_active": true,
//     "has_access_to_all_future_projects": false,
//     "default_hourly_rate": 150,
//     "department": "Ireland",
//     "wants_newsletter": true,
//     "updated_at": "2016-05-03T12:04:31Z",
//     "cost_rate": null,
//     "signup_redirection_cookie": null
// 	}
//
//  "task":
// {
//     "id": 5752604,
//     "name": "Bank Holiday",
//     "billable_by_default": false,
//     "created_at": "2016-03-22T12:02:12Z",
//     "updated_at": "2016-03-22T12:02:12Z",
//     "is_default": true,
//     "default_hourly_rate": 0,
//     "deactivated": false
// }
//
//
// "day_entry":
// {
//     "id": 439722386,
//     "notes": "Sheffeld co-lo, planning",
//     "spent_at": "2015-10-21",
//     "hours": 8,
//     "user_id": 1239662,
//     "project_id": 10284278,
//     "task_id": 5715688,
//     "created_at": "2016-03-15T17:00:48Z",
//     "updated_at": "2016-03-15T17:00:48Z",
//     "adjustment_record": false,
//     "timer_started_at": null,
//     "is_closed": false,
//     "is_billed": false
// }


function processProjects(projects) {
	logger.info("processProjects()");

	projects.forEach(function (aProject) {
		var harvestURL = config.get('harvest.url') + "projects/" + aProject.project.id + "/user_assignments";
	  logger.debug("harvestURL:["+harvestURL+"]");

		rest.get(harvestURL,
			{headers: utils.createBasicAuthHeader(config.get('harvest.encodedUser'))}
			).on('complete', function(data, response) {
				logger.info("Success reading " + data.length + " project users");
				if (data.length > 0) {
					db.storeProjectUsers(data);
					if (!(null==aProject.project.ends_on)) {
						db.forecastProjectTime(data, aProject.project.ends_on)
					}
				}
			}).on('fail', function(data, response) {
		  		logger.error("FAIL: " + response.statusCode + " MESSAGE " + data.errorMessages);
		  		return;
			}).on('error', function(data, response) {
		  		logger.error("ERROR: " + data.message + " / " + response);
		  		return;
			});

		var harvestURL2 = config.get('harvest.url') + "projects/" + aProject.project.id + "/entries?from=" + "2015-01-01" + "&to=" + utils.dateFormatIWant(new Date());
	  logger.debug("harvestURL:["+harvestURL2+"]");

		rest.get(harvestURL2,
			{headers: utils.createBasicAuthHeader(config.get('harvest.encodedUser'))}
			).on('complete', function(data, response) {
				logger.info("Success reading " + data.length + " project time entries");
				if (data.length > 0) {
					db.storeProjectTime(data);
				}
			}).on('fail', function(data, response) {
		  		logger.error("FAIL: " + response.statusCode + " MESSAGE " + data.errorMessages);
		  		return;
			}).on('error', function(data, response) {
		  		logger.error("ERROR: " + data.message + " / " + response);
		  		return;
			});
	});
}


exports.loadProjects = function() {
	logger.info("loadProjects()");

	var harvestURL = config.get('harvest.url') + "projects";
  logger.debug("harvestURL:["+harvestURL+"]");

	rest.get(harvestURL,
		{headers: utils.createBasicAuthHeader(config.get('harvest.encodedUser'))}
		).on('complete', function(data, response) {
			logger.info("Success reading " + data.length + " project entries");
			if (data.length > 0) {
				db.storeProjects(data);
				processProjects(data);
			}
		}).on('fail', function(data, response) {
	  		logger.error("FAIL: " + response.statusCode + " MESSAGE " + data.errorMessages);
	  		return;
		}).on('error', function(data, response) {
	  		logger.error("ERROR: " + data.message + " / " + response);
	  		return;
		});
}


exports.loadPeople = function() {
	logger.info("loadPeople()");

	var harvestURL = config.get('harvest.url') + "people";
  logger.debug("harvestURL:["+harvestURL+"]");

	rest.get(harvestURL,
		{headers: utils.createBasicAuthHeader(config.get('harvest.encodedUser'))}
		).on('complete', function(data, response) {
			logger.info("Success reading " + data.length + " people entries");
			if (data.length > 0) {
				db.storePeople(data);
			}
		}).on('fail', function(data, response) {
	  		logger.error("FAIL: " + response.statusCode + " MESSAGE " + data.errorMessages);
	  		return;
		}).on('error', function(data, response) {
	  		logger.error("ERROR: " + data.message + " / " + response);
	  		return;
		});
}


exports.loadTasks = function() {
	logger.info("loadTasks()");

	var harvestURL = config.get('harvest.url') + "tasks";
  logger.debug("harvestURL:["+harvestURL+"]");

	rest.get(harvestURL,
		{headers: utils.createBasicAuthHeader(config.get('harvest.encodedUser'))}
		).on('complete', function(data, response) {
			logger.info("Success reading " + data.length + " tasks entries");
			if (data.length > 0) {
				db.storeTasks(data);
			}
		}).on('fail', function(data, response) {
	  		logger.error("FAIL: " + response.statusCode + " MESSAGE " + data.errorMessages);
	  		return;
		}).on('error', function(data, response) {
	  		logger.error("ERROR: " + data.message + " / " + response);
	  		return;
		});
}


exports.loadCustomers = function() {
	logger.info("loadCustomers()");

	var harvestURL = config.get('harvest.url') + "clients";
  logger.debug("harvestURL:["+harvestURL+"]");

	rest.get(harvestURL,
		{headers: utils.createBasicAuthHeader(config.get('harvest.encodedUser'))}
		).on('complete', function(data, response) {
			logger.info("Success reading " + data.length + " client entries");
			if (data.length > 0) {
				db.storeCustomers(data);
			}
		}).on('fail', function(data, response) {
	  		logger.error("FAIL: " + response.statusCode + " MESSAGE " + data.errorMessages);
	  		return;
		}).on('error', function(data, response) {
	  		logger.error("ERROR: " + data.message + " / " + response);
	  		return;
		});
}


exports.loadInvoices = function() {
	logger.info("loadInvoices()");

	var harvestURL = config.get('harvest.url') + "invoices";
  logger.debug("harvestURL:["+harvestURL+"]");

	rest.get(harvestURL,
		{headers: utils.createBasicAuthHeader(config.get('harvest.encodedUser'))}
		).on('complete', function(data, response) {
			logger.info("Success reading " + data.length + " invoice entries");
			if (data.length > 0) {
				db.storeInvoices(data);
			}
		}).on('fail', function(data, response) {
	  		logger.error("FAIL: " + response.statusCode + " MESSAGE " + data.errorMessages);
	  		return;
		}).on('error', function(data, response) {
	  		logger.error("ERROR: " + data.message + " / " + response);
	  		return;
		});
}


exports.loadExpenses = function() {
	logger.info("loadExpenses()");

	var harvestURL = config.get('harvest.url') + "expenses";
	logger.debug("harvestURL:["+harvestURL+"]");

	rest.get(harvestURL,
		{headers: utils.createBasicAuthHeader(config.get('harvest.encodedUser'))}
		).on('complete', function(data, response) {
			logger.info("Success reading " + data.length + " expenses");
			if (data.length > 0) {
				db.storeInvoices(data);
			}
		}).on('fail', function(data, response) {
	  		logger.error("FAIL: " + response.statusCode + " MESSAGE " + data.errorMessages);
	  		return;
		}).on('error', function(data, response) {
	  		logger.error("ERROR: " + data.message + " / " + response);
	  		return;
		});
}
