'use strict';

var config = require('config');
var log4js = require('log4js');
var fileSystem = require('fs');
var utils = require("./loader_util");

log4js.configure('config/log4js_config.json', {});
var logger = log4js.getLogger();

exports.storeProjects = function(projects) {
	logger.info("storeProjects for " +  projects.length + " projects");

  var fileName = "./data/projects";
  fileSystem.writeFile(fileName, JSON.stringify(projects), (err) => {
    if (err) throw err;
  });
}

exports.storeProjectUsers = function(projectUsers) {
	logger.info("storeProjectUsers for " +  projectUsers.length + " project users");

  var fileName = "./data/assignments";
  fileSystem.appendFile(fileName, JSON.stringify(projectUsers), (err) => {
    if (err) throw err;
  });
}

exports.storeProjectTime = function(projectTime) {
	logger.info("storeProjectTime for " +  projectTime.length + " projectTime time entries");

  var fileName = "./data/time";
  fileSystem.appendFile(fileName, JSON.stringify(projectTime), (err) => {
    if (err) throw err;
  });
}
exports.forecastProjectTime = function(projectUsers, projectEndDate) {
  logger.info("NO OP Forecast");
}

exports.storePeople = function(people) {
	logger.info("storePeople for " +  people.length + " people");

  var fileName = "./data/employees";
  fileSystem.writeFile(fileName, JSON.stringify(people), (err) => {
    if (err) throw err;
  });
}

exports.storeTasks = function(tasks) {
	logger.info("storeTasks for " +  tasks.length + " tasks");

  var fileName = "./data/tasks";
  fileSystem.writeFile(fileName, JSON.stringify(tasks), (err) => {
    if (err) throw err;
  });
}

exports.storeCustomers = function(clients) {
	logger.info("storeCustomers for " +  clients.length + " customers");

  var fileName = "./data/customers";
  fileSystem.writeFile(fileName, JSON.stringify(clients), (err) => {
    if (err) throw err;
  });
}

exports.storeInvoices = function(invoices) {
	logger.info("storeInvoices for " +  invoices.length + " invoices");

  var fileName = "./data/invoices";
  fileSystem.writeFile(fileName, JSON.stringify(invoices), (err) => {
    if (err) throw err;
  });
}


exports.storeExpenses = function(expenses) {
	logger.info("storeExpenses for " +  expenses.length + " expenses");

  var fileName = "./data/expenses";
  fileSystem.writeFile(fileName, JSON.stringify(expenses), (err) => {
    if (err) throw err;
  });
}
