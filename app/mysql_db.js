'use strict';

var config = require('config');
var log4js = require('log4js');
var rest = require('restler');
var hashmap = require('hashmap');

log4js.configure('config/log4js_config.json', {});
var logger = log4js.getLogger();

var utils = require("./loader_util");

var mysql      = require('mysql');
var connection = mysql.createConnection({
  host     : config.get('mysql.host'),
  port     : config.get('mysql.port'),
  user     : config.get('mysql.user'),
  password : config.get('mysql.password'),
  database : config.get('mysql.db_name'),
  supportBigNumbers : true
});

var maxBigInt = 999999999999;
var defaultTaskId = 5715688;

function doIt(tableName, columNames, tableData, projectId, estimated) {

	var clearOut = "";

	if (null == projectId) {
		clearOut = mysql.format("DELETE FROM ??", tableName);
	} else {
		if (null == estimated) {
			var clearValues = [tableName, projectId];
			clearOut = mysql.format("DELETE FROM ?? WHERE project_id = ?", clearValues);
		} else {
			var clearValues = [tableName, projectId, estimated];
			clearOut = mysql.format("DELETE FROM ?? WHERE project_id = ? and is_estimated = ?", clearValues);
		}
	}
	logger.debug(clearOut);

	connection.query(clearOut, function(err, rows, fields) {
  		if (err) {
  			logger.error("Error emptying " + tableName + " ERROR: " + err);
  			throw err;
  		}
//  		logger.debug("rows " + JSON.stringify(rows));
//  		logger.debug("fields " + JSON.stringify(fields));
	});

	var sql = "INSERT INTO ?? (??) VALUES ?";
	var queryValues = [tableName, columNames, tableData];
	sql = mysql.format(sql, queryValues);

	// logger.debug("###### SQL START");
	// logger.debug(sql);
	// logger.debug("### SQL END");

	connection.query(sql, function(err, rows, fields) {
  		if (err) {
  			logger.error("Error saving " + tableName + " ERROR: " + err);
  			throw err;
  		}
//  		logger.debug("rows " + JSON.stringify(rows));
//  		logger.debug("fields " + JSON.stringify(fields));
	});
}

//     "id": 10281464,
//     "client_id": 4314423,
//     "name": "CD CI Phase 2",
//     "code": "",
//     "active": true,
//     "billable": true,
//     "bill_by": "People",
//     "hourly_rate": null,
//     "budget": 1528,
//     "budget_by": "project_cost",
//     "notify_when_over_budget": false,
//     "over_budget_notification_percentage": 80,
//     "over_budget_notified_at": null,
//     "show_budget_to_all": true,
//     "created_at": "2016-03-15T13:23:04Z",
//     "updated_at": "2016-04-16T16:36:16Z",
//     "starts_on": "2016-02-17",
//     "ends_on": "2016-05-18",
//     "estimate": 1528,
//     "estimate_by": "project_cost",
//     "hint_earliest_record_at": "2016-02-15",
//     "hint_latest_record_at": "2016-05-13",
//     "notes": "TRACE : OPP000138239  CRM: 50111504",
//     "cost_budget": 191815,
//     "cost_budget_include_expenses": false
exports.storeProjects = function(projects) {
	logger.info("storeProjects for " +  projects.length + " projects");

	var tableName = "project";
	var columNames = ['id', 'client_id', 'name', 'code', 'active', 'billable', 'bill_by', 'hourly_rate', 'budget', 'budget_by', 'notify_when_over_budget', 'over_budget_notification_percentage', 'over_budget_notified_at', 'show_budget_to_all', 'starts_on', 'ends_on', 'estimate', 'estimate_by', 'hint_earliest_record_at', 'hint_latest_record_at', 'notes', 'cost_budget', 'cost_budget_include_expenses', 'created_at', 'updated_at'];
	var tableData = [];
	projects.forEach(function(aProject) {
		var aRow =
			[aProject.project.id,
			aProject.project.client_id,
			aProject.project.name,
			aProject.project.code,
			aProject.project.active,
			aProject.project.billable,
			aProject.project.bill_by,
			aProject.project.hourly_rate,
			aProject.project.budget,
			aProject.project.budget_by,
			aProject.project.notify_when_over_budget,
			aProject.project.over_budget_notification_percentage,
			aProject.project.over_budget_notified_at,
			aProject.project.show_budget_to_all,
			aProject.project.starts_on,
			aProject.project.ends_on,
			aProject.project.estimate,
			aProject.project.estimate_by,
			aProject.project.hint_earliest_record_at,
			aProject.project.hint_latest_record_at,
			aProject.project.notes,
			aProject.project.cost_budget,
			aProject.project.cost_budget_include_expenses,
			new Date(aProject.project.created_at),
			new Date(aProject.project.updated_at)];

		tableData.push(aRow);
	});

	doIt(tableName, columNames, tableData);
}

    // {
    //     "user_id": 1239657,
    //     "project_id": 10284278,
    //     "is_project_manager": false,
    //     "deactivated": false,
    //     "hourly_rate": 125,
    //     "id": 85914544,
    //     "budget": null,
    //     "created_at": "2016-03-15T16:58:08Z",
    //     "updated_at": "2016-03-15T16:58:08Z",
    //     "estimate": null
    // }
exports.storeProjectUsers = function(projectUsers) {
	logger.info("storeProjectUserss for " +  projectUsers.length + " project users");

	var tableName = "assignment";
	var columNames = ['id', 'user_id', 'project_id', 'is_project_manager', 'deactivated',
		'hourly_rate', 'budget', 'estimate', 'created_at', 'updated_at'];
	var tableData = [];
	projectUsers.forEach(function(aUser) {
		var aRow = [aUser.user_assignment.id,
			aUser.user_assignment.user_id,
			aUser.user_assignment.project_id,
			aUser.user_assignment.is_project_manager,
			aUser.user_assignment.deactivated,
			aUser.user_assignment.hourly_rate,
			aUser.user_assignment.budget,
			aUser.user_assignment.estimate,
			new Date(aUser.user_assignment.created_at),
			new Date(aUser.user_assignment.updated_at)];

		tableData.push(aRow);
	});

	doIt(tableName, columNames, tableData, projectUsers[0].user_assignment.project_id);

}

    // {
    //     "id": 439722722,
    //     "notes": null,
    //     "spent_at": "2016-01-04",
    //     "hours": 8,
    //     "user_id": 1239657,
    //     "project_id": 10284278,
    //     "task_id": 5715688,
    //     "created_at": "2016-03-15T17:01:08Z",
    //     "updated_at": "2016-03-15T17:01:08Z",
    //     "adjustment_record": false,
    //     "timer_started_at": null,
    //     "is_closed": false,
    //     "is_billed": false
    // }
exports.storeProjectTime = function(projectTime) {
	logger.info("storeProjectTime for " +  projectTime.length + " projectTime time entries");

	var tableName = "effort";
	var columNames = ['user_id', 'project_id', 'task_id', 'hours', 'spent_at',
		'adjustment_record', 'timer_started_at', 'is_closed', 'is_billed', 'is_estimated',
		'notes', 'created_at', 'updated_at'];
	var tableData = [];
	projectTime.forEach(function(anEffort) {
		var aRow = [
      anEffort.day_entry.user_id,
			anEffort.day_entry.project_id,
			anEffort.day_entry.task_id,
			anEffort.day_entry.hours,
			anEffort.day_entry.spent_at,
			anEffort.day_entry.adjustment_record,
			anEffort.day_entry.timer_started_at,
			anEffort.day_entry.is_closed,
			anEffort.day_entry.is_billed,
			false,
			anEffort.day_entry.notes,
			new Date(anEffort.day_entry.created_at),
			new Date(anEffort.day_entry.updated_at)];

		tableData.push(aRow);
	});

	doIt(tableName, columNames, tableData, projectTime[0].day_entry.project_id, false);
}

exports.forecastProjectTime = function(projectUsers, projectEndDate) {

	var today = utils.dateFormatIWant((new Date()).toString());

	logger.info("forecastProjectTime for " +  projectUsers.length + " users from " + today + " to " + projectEndDate);

	var dateArray = utils.createDayArray(today, projectEndDate);

	if (dateArray.length < 1) return;

	var tableName = "effort";
	var columNames = ['user_id', 'project_id', 'task_id', 'hours', 'spent_at',
		'adjustment_record', 'timer_started_at', 'is_closed', 'is_billed', 'is_estimated',
		'notes', 'created_at', 'updated_at'];
	var tableData = [];
	projectUsers.forEach(function(aUser) {
		dateArray.forEach(function(aDate) {
			var aRow = [
				aUser.user_assignment.user_id,
				aUser.user_assignment.project_id,
				defaultTaskId,
				8,
				aDate,
				false,
				null,
				false,
				false,
				true,
				"ESTIMATED EFFORT via automation",
				new Date(),
				new Date()];

			tableData.push(aRow);

		});
	});

	doIt(tableName, columNames, tableData, projectUsers[0].user_assignment.project_id, true);
}


    // "id": 1244975,
    // "email": "A0052553@wipro.com",
    // "created_at": "2016-03-22T12:41:54Z",
    // "is_admin": true,
    // "first_name": "Alister",
    // "last_name": "Hodge",
    // "timezone": "London",
    // "is_contractor": true,
    // "telephone": "",
    // "is_active": true,
    // "has_access_to_all_future_projects": false,
    // "default_hourly_rate": 150,
    // "department": "Ireland",
    // "wants_newsletter": true,
    // "updated_at": "2016-05-03T12:04:31Z"
exports.storePeople = function(people) {
	logger.info("storePeople for " +  people.length + " people");

	var tableName = "people";
	var columNames = ['id', 'email', 'is_admin', 'first_name', 'last_name', 'timezone', 'is_contractor', 'telephone', 'is_active', 'has_access_to_all_future_projects', 'default_hourly_rate', 'department', 'wants_newsletter', 'created_at', 'updated_at'];
	var tableData = [];
	people.forEach(function(aPerson) {
		var aRow = [aPerson.user.id,
			aPerson.user.email,
			aPerson.user.is_admin,
			aPerson.user.first_name,
			aPerson.user.last_name,
			aPerson.user.timezone,
			aPerson.user.is_contractor,
			aPerson.user.telephone,
			aPerson.user.is_active,
			aPerson.user.has_access_to_all_future_projects,
			aPerson.user.default_hourly_rate,
			aPerson.user.department,
			aPerson.user.wants_newsletter,
			new Date(aPerson.user.created_at),
			new Date(aPerson.user.updated_at)];

		tableData.push(aRow);
	});

	doIt(tableName, columNames, tableData);
}

    // {
    //     "id": 5752604,
    //     "name": "Bank Holiday",
    //     "billable_by_default": false,
    //     "created_at": "2016-03-22T12:02:12Z",
    //     "updated_at": "2016-03-22T12:02:12Z",
    //     "is_default": true,
    //     "default_hourly_rate": 0,
    //     "deactivated": false
    // }
exports.storeTasks = function(tasks) {
	logger.info("storeTasks for " +  tasks.length + " tasks");

	var tableName = "task";
	var columNames = ['id', 'name', 'billable_by_default', 'is_default', 'default_hourly_rate', 'deactivated',
		'created_at', 'updated_at'];
	var tableData = [];
	tasks.forEach(function(aTask) {
		var aRow = [aTask.task.id,
			aTask.task.name,
			aTask.task.billable_by_default,
			aTask.task.is_default,
			aTask.task.default_hourly_rate,
			aTask.task.deactivated,
			new Date(aTask.task.created_at),
			new Date(aTask.task.updated_at)];

		tableData.push(aRow);
	});

	doIt(tableName, columNames, tableData);
}

    // {
    //     "id": 4314426,
    //     "name": "Allied Irish Bank",
    //     "active": true,
    //     "currency": "Euro - EUR",
    //     "highrise_id": null,
    //     "cache_version": 1731865124,
    //     "updated_at": "2016-03-22T13:04:17Z",
    //     "created_at": "2016-03-15T13:09:48Z",
    //     "statement_key": "1b40efb65f8918651d304b3bce662001",
    //     "default_invoice_kind": null,
    //     "default_invoice_timeframe": null,
    //     "address": "",
    //     "currency_symbol": "€",
    //     "details": "",
    //     "last_invoice_kind": null
    // }
exports.storeCustomers = function(clients) {
	logger.info("storeCustomers for " +  clients.length + " customers");

	var tableName = "client";
	var columNames = ['id', 'name', 'active', 'currency', 'highrise_id', 'cache_version',
		'statement_key', 'default_invoice_kind', 'default_invoice_timeframe', 'address', 'currency_symbol',
		'details', 'last_invoice_kind', 'created_at', 'updated_at'];
	var tableData = [];
	clients.forEach(function(aCustomer) {
		var aRow = [aCustomer.client.id,
			aCustomer.client.name,
			aCustomer.client.active,
			aCustomer.client.currency,
			aCustomer.client.highrise_id,
			aCustomer.client.cache_version,
			aCustomer.client.statement_key,
			aCustomer.client.default_invoice_kind,
			aCustomer.client.default_invoice_timeframe,
			aCustomer.client.address,
			aCustomer.client.currency_symbol,
			aCustomer.client.details,
			aCustomer.client.last_invoice_kind,
			new Date(aCustomer.client.created_at),
			new Date(aCustomer.client.updated_at)];

		tableData.push(aRow);
	});

	doIt(tableName, columNames, tableData);
}



    // {
    //     "id": 9490183,
    //     "client_id": 4314423,
    //     "period_start": "2016-03-21",
    //     "period_end": "2016-04-20",
    //     "number": "1",
    //     "issued_at": "2016-04-27",
    //     "due_at": "2016-04-27",
    //     "amount": 69650.04,
    //     "currency": "British Pound - GBP",
    //     "state": "draft",
    //     "notes": "",
    //     "purchase_order": "",
    //     "due_amount": 69650.04,
    //     "due_at_human_format": "upon receipt",
    //     "created_at": "2016-04-27T17:06:00Z",
    //     "updated_at": "2016-04-27T17:06:00Z",
    //     "tax": null,
    //     "tax_amount": 0,
    //     "subject": "HSBC CI/CD invoice 21 Mar - 20 Apr 2016",
    //     "recurring_invoice_id": null,
    //     "tax2": null,
    //     "tax2_amount": 0,
    //     "client_key": "06b6affdf8d523e0dc812338e07054a4a78661e3",
    //     "estimate_id": null,
    //     "discount": null,
    //     "discount_amount": 0,
    //     "retainer_id": null,
    //     "created_by_id": 1239646,
    //     "project_id": null,
    //     "client_name": "HSBC"
    // }
exports.storeInvoices = function(invoices) {
	logger.info("storeInvoices for " +  invoices.length + " invoices");

	var tableName = "invoice";
	var columNames = ['id', 'client_id', 'period_start', 'period_end', 'number', 'issued_at',
	'due_at', 'amount', 'currency', 'state', 'notes', 'purchase_order', 'due_amount',
	'due_at_human_format', 'tax', 'tax_amount', 'subject', 'recurring_invoice_id',
	'tax2', 'tax2_amount', 'client_key', 'estimate_id', 'discount', 'discount_amount',
	'retainer_id', 'created_by_id', 'project_id', 'client_name', 'created_at', 'updated_at'];
	var tableData = [];
	invoices.forEach(function(anInvoice) {
		var aRow = [anInvoice.invoices.id,
			anInvoice.invoices.client_id,
			anInvoice.invoices.period_start,
			anInvoice.invoices.period_end,
			anInvoice.invoices.number,
			anInvoice.invoices.issued_at,
			anInvoice.invoices.due_at,
			anInvoice.invoices.amount,
			anInvoice.invoices.currency,
			anInvoice.invoices.state,
			anInvoice.invoices.notes,
			anInvoice.invoices.purchase_order,
			anInvoice.invoices.due_amount,
			anInvoice.invoices.due_at_human_format,
			anInvoice.invoices.tax,
			anInvoice.invoices.tax_amount,
			anInvoice.invoices.subject,
			anInvoice.invoices.recurring_invoice_id,
			anInvoice.invoices.tax2,
			anInvoice.invoices.tax2_amount,
			anInvoice.invoices.client_key,
			anInvoice.invoices.estimate_id,
			anInvoice.invoices.discount,
			anInvoice.invoices.discount_amount,
			anInvoice.invoices.retainer_id,
			anInvoice.invoices.created_by_id,
			anInvoice.invoices.project_id,
			anInvoice.invoices.client_name,
			new Date(anInvoice.invoices.created_at),
			new Date(anInvoice.invoices.updated_at)];

		tableData.push(aRow);
	});

	doIt(tableName, columNames, tableData);
}

exports.storeExpenses = function(expenses) {
	logger.info("storeExpenses for " +  expenses.length + " expenses");
}
