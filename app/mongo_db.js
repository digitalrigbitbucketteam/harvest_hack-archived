'use strict';

var config = require('config');
var log4js = require('log4js');
var rest = require('restler');
var hashmap = require('hashmap');

var MongoClient = require('mongodb').MongoClient;
var assert = require('assert');

log4js.configure('config/log4js_config.json', {});
var logger = log4js.getLogger();

var utils = require("./loader_util");

exports.storeProjects = function(projects) {
	logger.info("storeProjects for " +  projects.length + " projects");

	var url = config.get('datastore.dbUrl') + "/harvest";

	MongoClient.connect(url, function(err, db) {
	  var col = db.collection('projects');
	  col.insertMany(projects).then(function(r) {
	    assert.equal(projects.length, r.insertedCount);
	    db.close();
	  });
	});

}

exports.storeProjectUsers = function(projectUsers) {
	logger.info("storeProjectUserss for " +  projectUsers.length + " project users");

	var url = config.get('datastore.dbUrl') + "/harvest";

	MongoClient.connect(url, function(err, db) {
	  var col = db.collection('projectUsers');
	  col.insertMany(projectUsers).then(function(r) {
	    assert.equal(projectUsers.length, r.insertedCount);
	    db.close();
	  });
	});

}

exports.storeProjectTime = function(projectTime) {
	logger.info("storeProjectTime for " +  projectTime.length + " projectTime time entries");

	var url = config.get('datastore.dbUrl') + "/harvest";

	MongoClient.connect(url, function(err, db) {
	  var col = db.collection('projectTime');
	  col.insertMany(projectTime).then(function(r) {
	    assert.equal(projectTime.length, r.insertedCount);
	    db.close();
	  });
	});

}

exports.storePeople = function(people) {
	logger.info("storePeople for " +  people.length + " people");

	var url = config.get('datastore.dbUrl') + "/harvest";

	MongoClient.connect(url, function(err, db) {
	  var col = db.collection('people');
	  col.insertMany(people).then(function(r) {
	    assert.equal(people.length, r.insertedCount);
	    db.close();
	  });
	});

}

exports.storeTasks = function(tasks) {
	logger.info("storeTasks for " +  tasks.length + " tasks");

	var url = config.get('datastore.dbUrl') + "/harvest";

	MongoClient.connect(url, function(err, db) {
	  var col = db.collection('tasks');
	  col.insertMany(tasks).then(function(r) {
	    assert.equal(tasks.length, r.insertedCount);
	    db.close();
	  });
	});

}

exports.storeCustomers = function(clients) {
	logger.info("storeCustomers for " +  clients.length + " customers");

	var url = config.get('datastore.dbUrl') + "/harvest";

	MongoClient.connect(url, function(err, db) {
	  var col = db.collection('clients');
	  col.insertMany(clients).then(function(r) {
	    assert.equal(clients.length, r.insertedCount);
	    db.close();
	  });
	});

}

exports.storeInvoices = function(invoices) {
	logger.info("storeInvoices for " +  invoices.length + " invoices");

	var url = config.get('datastore.dbUrl') + "/harvest";

	MongoClient.connect(url, function(err, db) {
	  var col = db.collection('invoices');
	  col.insertMany(invoices).then(function(r) {
	    assert.equal(invoices.length, r.insertedCount);
	    db.close();
	  });
	});

}


exports.storeExpenses = function(expenses) {
	logger.info("storeExpenses for " +  expenses.length + " expenses");

	var url = config.get('datastore.dbUrl') + "/harvest";

	MongoClient.connect(url, function(err, db) {
	  var col = db.collection('expenses');
	  col.insertMany(expenses).then(function(r) {
	    assert.equal(expenses.length, r.insertedCount);
	    db.close();
	  });
	});

}
