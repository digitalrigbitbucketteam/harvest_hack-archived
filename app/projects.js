'use strict';

var config = require('config');
var log4js = require('log4js');

log4js.configure('config/log4js_config.json', {});
var logger = log4js.getLogger();

var harvest = require("./effort_harvest");


harvest.loadProjects();
harvest.loadPeople();
harvest.loadTasks();
harvest.loadCustomers();
harvest.loadInvoices();
harvest.loadExpenses();
