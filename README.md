# HARVEST_Hack [![NPM version][npm-image]][npm-url] [![Build Status][travis-image]][travis-url] [![Dependency Status][daviddm-image]][daviddm-url]
> Use node to extract data from Jira

## PURPOSE
## 
## This project is extracts data from Harvest via its REST API and then inserts the data into a MySQL database.
## In theory this allows us to use Chart.io to analyze and trend the data.
## This code also forecasts future effort data to allow for planning of assignments and revenue.
## 

## INSTALLATION
## 
## These utilites require a data store.  At this time that is MongoDB.
## Please install, configure, and have it running prior to useing them.
## Then use the config/default.json to set the mongodb URL
##
## Prior to useing or developing run the command below to load all node.js requirements

```sh
$ npm install --save
```


## Usage
## 
## CAUTION : This assumes that you have created the database and schema as described in the harvest_schema.sql file.

```sh
$ node app/projects.js
```

##
## This project uses log4js.  There is a config file that specifically for that.
## It is handy to have console logging on when debugging so add the following into the appenders list.
	{
		"type": "console",
		"layout": {
			  "type": "pattern",
			  "pattern": "%d{ABSOLUTE} %[%-5p%] %c %m"
		}
	}


## License

 © [Buildit]()